package main

import (
	"bytes"
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"strings"
	"sync"
	"syscall"
	"time"

	"filippo.io/age"
	"filippo.io/age/agessh"
	"github.com/go-co-op/gocron/v2"
	// include Pg
	_ "github.com/lib/pq"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/robfig/cron/v3"
	"golang.org/x/crypto/ssh"
	"sigs.k8s.io/yaml"

	"gitlab.com/BobyMCbobs/pg-s3-backup/pkg/config"
)

var DefaultSchedule = `0 0 * * */4`

type app struct {
	mc        *minio.Client
	recipient *agessh.Ed25519Recipient
	scheduler gocron.Scheduler
	cron      cron.Schedule

	config *config.Config
}

func NewApp(c *config.Config) (*app, error) {
	c.S3Storage.AccessKey = strings.Trim(os.ExpandEnv(c.S3Storage.AccessKey), " ")
	c.S3Storage.SecretKey = strings.Trim(os.ExpandEnv(c.S3Storage.SecretKey), " ")
	for i, v := range c.Hosts {
		c.Hosts[i].URL = os.ExpandEnv(v.URL)
	}
	secure := true
	if c.S3Storage.Secure != nil && !*c.S3Storage.Secure {
		secure = false
	}
	minioClient, err := minio.New(c.S3Storage.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(c.S3Storage.AccessKey, c.S3Storage.SecretKey, ""),
		Secure: secure,
	})
	if err != nil {
		return nil, err
	}
	pubKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(c.AgePublicKey))
	if err != nil {
		return nil, err
	}
	recipient, err := agessh.NewEd25519Recipient(pubKey)
	if err != nil {
		return nil, err
	}
	s, err := gocron.NewScheduler()
	if err != nil {
		return nil, err
	}
	if c.Schedule == "" {
		c.Schedule = DefaultSchedule
	}
	cron, err := cron.ParseStandard(c.Schedule)
	if err != nil {
		return nil, fmt.Errorf("error failed to parse schedule: %v", err)
	}
	return &app{
		mc:        minioClient,
		recipient: recipient,
		scheduler: s,
		cron:      cron,

		config: c,
	}, nil
}

func runCmd(out *bytes.Buffer, cmd ...string) error {
	c := exec.Command(cmd[0], cmd[1:]...)
	c.Stdout = out
	if err := c.Run(); err != nil {
		return err
	}
	return nil
}

func Ping(db *sql.DB) (err error) {
	var zero int
	rows, err := db.Query(`SELECT 0`)
	if err != nil {
		log.Println("Error querying database", err.Error())
		return err
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.Printf("error: failed to close rows: %v\n", err)
		}
	}()
	rows.Next()
	if err := rows.Scan(&zero); err != nil {
		return err
	}
	if err := rows.Err(); err != nil {
		return err
	}
	if zero != 0 {
		return fmt.Errorf("wild, this error should never occur")
	}
	return nil
}

func (a *app) PingTest() error {
	hostsFailed := []string{}
	for _, v := range a.config.Hosts {
		db, err := sql.Open("postgres", v.URL)
		if err != nil {
			return err
		}
		if err := Ping(db); err != nil {
			hostsFailed = append(hostsFailed, v.Name)
		}
	}
	if len(hostsFailed) > 0 {
		return fmt.Errorf("error: failed to ping hosts: %v", hostsFailed)
	}
	return nil
}

func (a *app) Backup(host config.Host) error {
	stdout, backup := &bytes.Buffer{}, &bytes.Buffer{}
	log.Printf("... [%v] dumping database\n", host.Name)
	if host.OverrideDatabaseName != nil {
		u, err := url.Parse(host.URL)
		if err != nil {
			return fmt.Errorf("failed to parse database connection string for %v: %v", host.Name, err)
		}
		u.Path = *host.OverrideDatabaseName
		host.URL = u.String()
	}
	if err := runCmd(stdout, "pg_dump", host.URL); err != nil {
		return fmt.Errorf("error dumping database: %+v\n", err)
	}
	w, err := age.Encrypt(backup, a.recipient)
	if err != nil {
		return fmt.Errorf("failed to create encrypted file: %v", err)
	}
	log.Printf("... [%v] encrypting\n", host.Name)
	if _, err := io.WriteString(w, stdout.String()); err != nil {
		return fmt.Errorf("failed to write to encrypted file: %v", err)
	}
	if err := w.Close(); err != nil {
		return err
	}
	datestamp := time.Now().UTC().Format("20060102150405")
	objectName := fmt.Sprintf("%v-%v", host.Name, datestamp)
	ctx := context.Background()
	log.Printf("... [%v] uploading\n", host.Name)
	info, err := a.mc.PutObject(
		ctx,
		a.config.S3Storage.DestinationPath,
		path.Join("/", objectName+".sql.age"),
		backup,
		int64(backup.Len()),
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("error putting object: %+v\n", err)
	}
	log.Printf("... [%v] uploaded %v/%v\n", host.Name, a.config.S3Storage.DestinationPath, info.Key)
	stdout, backup = nil, nil
	return nil
}

func (a *app) BackupAllHosts() error {
	var errs []error
	var mu sync.Mutex
	var wg sync.WaitGroup
	for _, h := range a.config.Hosts {
		wg.Add(1)
		go func(h config.Host) {
			log.Printf("... [%v] backing up\n", h.Name)
			if err := a.Backup(h); err != nil {
				mu.Lock()
				errs = append(errs, fmt.Errorf("error performing a backup: %+v\n", err))
				defer mu.Unlock()
			}
			wg.Done()
		}(h)
	}
	wg.Wait()
	if len(errs) > 0 {
		return fmt.Errorf("errors backing up hosts: %v", errs)
	}
	log.Printf("... done")
	return nil
}

func main() {
	log.Println("launching pg-s3-backup...")
	var filePath string
	var singleShot bool
	flag.StringVar(&filePath, "config", "config.yaml", "path to a config file")
	flag.BoolVar(&singleShot, "single-shot", false, "run once and exit")
	flag.Parse()

	var file []byte
	isEnv := strings.HasPrefix(filePath, "$")
	if isEnv {
		file = []byte(os.ExpandEnv(filePath))
	} else {
		var err error
		file, err = os.ReadFile(filePath)
		if err != nil {
			log.Fatalf("error reading file: %+v\n", err)
		}
	}

	cfg := &config.Config{}
	if err := yaml.Unmarshal(file, cfg); err != nil {
		log.Fatalf("error unmarshalling file: %+v\n", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	app, err := NewApp(cfg)
	if err != nil {
		log.Fatalf("error setting up runtime: %+v\n", err)
	}
	if err := app.config.Validate(); err != nil {
		log.Fatalf("error validating config: %+v\n", err)
	}
	if err := app.PingTest(); err != nil {
		log.Fatalf("error pinging databases: %v\n", err)
	}
	if _, err := app.scheduler.NewJob(
		gocron.CronJob(app.config.Schedule, false),
		gocron.NewTask(func() {
			if err := app.BackupAllHosts(); err != nil {
				log.Printf("... backup failed: %v\n", err)
			}
			log.Printf("... next run: %v", app.cron.Next(time.Now()).String())
		}),
	); err != nil {
		log.Fatalf("error setting up cronjob: %+v\n", err)
	}
	if singleShot {
		if err := app.BackupAllHosts(); err != nil {
			log.Fatalf("error backing up all hosts: %v\n", err)
		}
		return
	}
	log.Printf("running scheduler (%v)...\n", app.config.Schedule)
	app.scheduler.Start()
	log.Printf("... next run: %v", app.cron.Next(time.Now()).String())

	<-done
	log.Println("Shutting down...")
}
