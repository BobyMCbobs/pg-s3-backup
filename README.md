# pg s3 backup

> declaratively schedule S3 backups

# Purpose

Have a single running service which performs scheduled and encrypted backups to store in S3-like storage.

# Usage

Create a config file like

```yaml
---
agePublicKey: |
  ssh-ed25519 ...
hosts:
  - name: local
    url: $PGURL
s3Storage:
  endpoint: localhost:9000
  accessKey: $ACCESS_KEY
  secretKey: $SECRET_KEY
  destinationPath: backups
schedule: '* * * * *'
```

Run with

```sh
podman run -it --rm \
  -v config.yaml:config.yaml,Z \
  registry.gitlab.com/bobymcbobs/pg-s3-backup:latest --help
```

# Configuration

**Config file fields**

| Name                         | Description                                                                                               | Notes                                                  |
|------------------------------|-----------------------------------------------------------------------------------------------------------|--------------------------------------------------------|
| `agePublicKey`               | an [age](https://age-encryption.org) support public key for encryption, such an an ed25519 SSH public key |                                                        |
| `hosts.name`                 | a lowercase short name for the backup                                                                     |                                                        |
| `hosts.url`                  | the postgres url                                                                                          | supports expand from shell variable beginning with `$` |
| `hosts.overrideDatabaseName` | override the database field in a provided url                                                             |                                                        |
| `s3Storage.accessKey`        | the access key for S3-compatible storage                                                                  | supports expand from shell variable beginning with `$` |
| `s3Storage.destinationPath`  | the path to use to write in the S3-compatible bucket                                                      |                                                        |
| `s3Storage.endpoint`         | the endpoint for the S3-compatible storage                                                                |                                                        |
| `s3Storage.secretKey`        | the secret key for S3-compatible storage                                                                  | supports expand from shell variable beginning with `$` |
| `s3Storage.secure`           | use a secure protocol to access the storage bucket                                                        | defaults to `true`                                     |
| `schedule`                   | the cron schedule for running continuously                                                                |                                                        |

**Program args**

| Name            | Description                                                                    | Default       |
|-----------------|--------------------------------------------------------------------------------|---------------|
| `--config`      | path to config or shell variable beginning with `$` to directly eval data from | `config.yaml` |
| `--single-shot` | whether to run once and exit                                                   | `false`       |

# Images

Verify images with [cosign](https://docs.sigstore.dev/about/overview/)

```sh
cosign verify \
  --certificate-oidc-issuer 'https://gitlab.com' \
  --certificate-identity-regexp 'https://gitlab.com/BobyMCbobs/pg-s3-backup//.gitlab-ci.yml@refs/(heads/main|tags/*)' \
  -o text \
  registry.gitlab.com/bobymcbobs/pg-s3-backup:latest
```

# Notes

- depends on `pg_dump`, otherwise included in the container image

## License

Copyright 2024 Caleb Woodbine.
This project is licensed under the [AGPL-3.0](http://www.gnu.org/licenses/agpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).
This program comes with absolutely no warranty.
