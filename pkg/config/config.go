package config

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"regexp"

	"github.com/robfig/cron/v3"
	"sigs.k8s.io/yaml"
)

var hostNameRegexp = regexp.MustCompile("^[a-z_-]+$")

type Host struct {
	Name                 string  `json:"name"`
	URL                  string  `json:"url"`
	OverrideDatabaseName *string `json:"overrideDatabaseName"`
}
type S3Storage struct {
	Endpoint        string `json:"endpoint"`
	AccessKey       string `json:"accessKey"`
	SecretKey       string `json:"secretKey"`
	DestinationPath string `json:"destinationPath"`
	Secure          *bool  `json:"secure"`
}
type Config struct {
	Hosts        []Host    `json:"hosts"`
	S3Storage    S3Storage `json:"s3Storage"`
	AgePublicKey string    `json:"agePublicKey"`
	Schedule     string    `json:"schedule"`
}

func (c *Config) Validate() error {
	if len(c.Hosts) == 0 {
		return fmt.Errorf("error at least one host in hosts must be provided")
	}
	for i, v := range c.Hosts {
		_, err := url.Parse(os.ExpandEnv(v.URL))
		if err != nil {
			return err
		}
		if !hostNameRegexp.MatchString(v.Name) {
			return fmt.Errorf("error host name: %v does not match %v", v.Name, hostNameRegexp.String())
		}
		for ii, vv := range c.Hosts {
			if i != ii && v.Name == vv.Name {
				return fmt.Errorf("error host name values must be different: [%v]='%v' == [%v]='%v'", i, v.Name, ii, vv.Name)
			}
		}
	}
	if _, err := url.Parse(c.S3Storage.Endpoint); err != nil {
		return fmt.Errorf("error parsing s3storage endpoint URL: %v", err)
	}
	if c.S3Storage.Endpoint == "" {
		return fmt.Errorf("error s3 storage endpoint key must be set")
	}
	if c.S3Storage.AccessKey == "" {
		return fmt.Errorf("error s3 storage access key must be set")
	}
	if c.S3Storage.SecretKey == "" {
		return fmt.Errorf("error s3 storage secret key must be set")
	}
	if c.S3Storage.DestinationPath == "" {
		return fmt.Errorf("error s3 storage destination path must be set")
	}
	if c.AgePublicKey == "" {
		return fmt.Errorf("error destination path must be set")
	}
	if _, err := cron.ParseStandard(c.Schedule); err != nil {
		return fmt.Errorf("error failed to parse schedule: %v", err)
	}
	return nil
}

func (c *Config) String() string {
	s, err := yaml.Marshal(c)
	if err != nil {
		log.Fatalf("error unmarshalling file: %+v\n", err)
	}
	return string(s)
}
